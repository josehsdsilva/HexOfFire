﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hexagon : MonoBehaviour
{
    Vector3 pos;
    int type, subtype;
    Transform objectDrawn;
    public List<Transform> prefabs;
    int animationStatus = 0;
    float Speed = 0.75f;
    int hexTypes = 3; 

    public Vector3 Pos
    {
        get { return pos; }
        set { pos = value; }
    }

    public int Type
    {
        get { return type; }
    }

    /*
        10 - hex
        20 - player
        30 - fire
        100 - fallHex
    */

    public void DrawObject(int _type)
    {
        type = _type;

        int auxType = type / 10;
        int auxSubType = type % 10;

        if (objectDrawn != null)
        {
            Destroy(objectDrawn.gameObject);
        }
        if(auxType > 0 && auxType < 10)
        {
            objectDrawn = Instantiate(prefabs[(auxType - 1) * hexTypes + auxSubType], new Vector3(pos.x, pos.y, pos.z), Quaternion.identity, this.transform);
        }
        else if (auxType == 10)
        {
            objectDrawn = Instantiate(prefabs[auxSubType], new Vector3(pos.x, pos.y, pos.z), Quaternion.identity, this.transform);
            animationStatus = 1;
            Speed = 0.75f;
        }
    }

    public int RandomizeType()
    {
        int random1, random2, random3;
        random1 = Random.Range(0, 100);
        random2 = Random.Range(0, 30);
        random3 = Random.Range(0, 3);
        if (random1 >= 40)
        {
            if (random2 < 5)
            {
                return 30 + random3;
            }
            else
            {
                return 10 + random3;
            }
        }
        else if(random1 >= 10)
        {
            if (random2 < 15)
            {
                return 30 + random3;
            }
            else
            {
                return 10 + random3;
            }
        }
        else
        {
            if (random2 < 20)
            {
                return 30 + random3;
            }
            else
            {
                return 10 + random3;
            }
        }
    }

    private void FixedUpdate()
    {
        if(type/10 == 10)
        {
            if(animationStatus == 1)
            {
                pos.y -= Time.deltaTime * Speed;
                Speed *= 1.15f;
                if (pos.y < -50)
                {
                    Destroy(objectDrawn.gameObject);
                    animationStatus = 2;
                }
            }
        }
        if(objectDrawn != null)
        {
            objectDrawn.position = new Vector3(pos.x, pos.y, pos.z);
        }
    }
}
