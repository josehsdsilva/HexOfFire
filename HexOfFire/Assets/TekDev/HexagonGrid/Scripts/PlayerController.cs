﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    int x, y, oldY, mapWidth, aux = 0, score;
    public Text scoreText;

    private void Start()
    {
        x = 16;
        y = 3;
        mapWidth = 31;
        score = 0;
        scoreText.text = "Score: " + score;
    }

    public void UpdateInfo()
    {
        y--;
        aux++;
        if (aux == 2) aux = 0;
    }

    public void Movement(int button)
    {
        oldY = y;
        if (GridManager.instance.gameState == 100)
        {
            if (button == 0)
            {
                if (x - 1 >= 0)
                {
                    MoveAction(0, -1);
                }
            }
            else if (button == 1)
            {
                if ((y + aux) % 2 == 1)
                {
                    MoveAction(1, 0);
                }
                else
                {
                    if (x - 1 >= 0)
                    {
                        MoveAction(1, -1);
                    }
                }
            }
            else if (button == 2)
            {
                if ((y + aux) % 2 == 0)
                {
                    MoveAction(1, 0);
                }
                else
                {
                    if (x - 1 >= 0)
                    {
                        MoveAction(1, 1);
                    }
                }
            }
            else if (button == 3)
            {
                if (x + 1 < mapWidth)
                {
                    MoveAction(0, 1);
                }
            }
        }
        if(y > oldY)
        {
            score += 10;
        }
        scoreText.text = "Score: " + score;
    }

    void MoveAction(int movY, int movX)
    {
        ReplaceCurrentPosition();
        if (GetHexType(y + movY, x + movX) / 10 > 2)
        {
            SetGameOver();
        }
        else
        {
            ReplaceOnMove(movY, movX);
        }
    }

    void ReplaceCurrentPosition()
    {
        int subType = GetHexType(y, x) % 10;
        GridManager.instance.grid[y][x].DrawObject(100 + subType);
    }

    void ReplaceOnMove(int movY, int movX)
    {
        int subType = GetHexType(y + movY, x + movX) % 10;
        GridManager.instance.grid[y + movY][x + movX].DrawObject(20 + subType);
        y += movY;
        x += movX;
    }

    void SetGameOver()
    {
        GridManager.instance.gameState = 1000;
    }

    int GetHexType(int movY, int movX)
    {
        return GridManager.instance.grid[movY][movX].Type;
    }
}
