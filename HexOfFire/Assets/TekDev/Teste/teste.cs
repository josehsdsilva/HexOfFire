﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class teste : MonoBehaviour
{
    int[] B = { 1, 3, 6, 4, 1, 2, 5, 7 };
    // Start is called before the first frame update
    void Start()
    {
        //Solution(B);
        //Debug.Log(Solution(B));

        Debug.Log(turbulance(A3));
    }

    public int Solution(int[] A)
    {
        // write your code in C# 6.0 with .NET 4.5 (Mono)
        int aux;
        int aux2 = 0;
        for (int j = 0; j < A.Length; j++)
        {
            for (int i = 0; i < A.Length; i++)
            {
                if (i < A.Length - 1 && A[i] > A[i + 1] && i != j)
                {
                    aux = A[i];
                    A[i] = A[i + 1];
                    A[i + 1] = aux;
                }
            }
        }

        //for (int i = 0; i < A.Length; i++)
        //{
        //    Debug.Log(A[i]);
        //}

        aux2 = giveNum(A, 1);
        return aux2;
    }

    int giveNum(int[] A, int _num)
    {
        for (int i = 0; i < A.Length; i++)
        {
            if (A[i] == _num)
            {
                return giveNum(A, _num + 1);
            }   
        }

        return _num;
    }
    //Write a function:

    //class Solution { public int solution(int[] A); }

    //    that, given an array A of N integers, returns the smallest positive integer(greater than 0) that does not occur in A.

    //   For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

    //Given A = [1, 2, 3], the function should return 4.

    //Given A = [−1, −3], the function should return 1.

    //Write an efficient algorithm for the following assumptions:

    //N is an integer within the range[1..100, 000];
    //each element of array A is an integer within the range[−1, 000, 000..1, 000, 000].

    int[] A1 = { 9, 4, 2, 10, 7, 8, 8, 1, 9 };
    int[] A2 = { 4, 8, 12, 16 };
    int[] A3 = { 100 };

    int turbulance(int[] A)
    {
        int max = 1, newMax;
        for(int i = 0; i < A.Length; i++)
        {
            if (i < A.Length - 1)
            {
                newMax = period(A, greater(A[i + 1], A[i]), i + 1, 1);
                i += newMax - 2;
                if (newMax > max)
                {
                    max = newMax;
                }
            }
        }
        return max;
    }

    int period(int[] A, int up, int start, int count)
    {
        int aux_count = count + 1;
        if (start < A.Length - 1)
        {
            if (up == 1)
            {
                if (greater(A[start + 1], A[start]) == -1)
                {
                    return period(A, greater(A[start + 1], A[start]), start + 1, aux_count);
                }
            }
            else if(up == -1)
            {
                if(greater(A[start + 1], A[start]) == 1)
                {
                    return period(A, greater(A[start + 1], A[start]), start + 1, aux_count);
                }
            }
        }
        return aux_count;
    }

    int greater(int a, int b)
    {
        if (a > b) return 1;
        else if (a < b) return -1;
        return 0;
    }

    // u = 3, L = 2, C = {2, 1, 1, 0, 1}

    // 1 1 0 0 1
    // 1 0 1 0 0

    // 11001,10100

    public string e3(int U, int L, int[] C)
    {
        string aux_string = "";
        int[,] aux_array = new int[2, C.Length];
        int newU = U, newL = L;

        int counter = 0;

        for (int i = 0; i < C.Length; i++)
        {
            counter += C[i];
            if(counter != U+L) return "IMPOSSIBLE";
        }

        for (int i = 0; i < C.Length; i++)
        {
            aux_array[0, i] = -1;
            aux_array[1, i] = -1;
        }

        for (int i = 0; i < C.Length; i++)
        {
            if (C[i] == 2)
            {
                aux_array[0, i] = 1;
                aux_array[1, i] = 1;
                newU--;
                newL--;
            }
            else if (C[i] == 0)
            {
                aux_array[0, i] = 0;
                aux_array[1, i] = 0;
            }
        }

        for (int i = 0; i < C.Length; i++)
        {
            if (aux_array[1, i] == -1 && newL > 0)
            {
                aux_array[1, i] = 1;
                aux_array[0, i] = 0;
                newL--;
            } 
            if (aux_array[0, i] == -1 && newU > 0)
            {
                aux_array[0, i] = 1;
                aux_array[1, i] = 0;
                newU--;
            }
            if(newL < 0 || newU < 0) return "IMPOSSIBLE";
        }

        for (int j = 0; j < 2; j++)
        {
            for (int i = 0; i < C.Length; i++)
            {
                if (aux_array[j, i] == -1)
                {
                    return "IMPOSSIBLE";
                }
                aux_string += aux_array[j, i];
            }
            if (j == 0) aux_string += ",";
        }

        return aux_string;
    }
}
